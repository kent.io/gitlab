export const mockMilestones = [
  {
    id: 41,
    title: 'Sprint - Eligendi et aut pariatur ab rerum vel.',
    project_id: 1,
    description: 'Accusamus qui sapiente porro et in voluptates.',
    due_date: '2020-01-14',
    created_at: '2020-01-08T15:47:37.697Z',
    updated_at: '2020-01-08T15:47:37.697Z',
    state: 'active',
    iid: 6,
    start_date: '2020-01-08',
    group_id: null,
    name: 'Sprint - Eligendi et aut pariatur ab rerum vel.',
  },
  {
    id: 5,
    title: 'v4.0',
    project_id: 1,
    description: 'Atque laudantium reiciendis consequatur temporibus qui qui.',
    due_date: null,
    created_at: '2020-01-18T15:46:07.448Z',
    updated_at: '2020-01-18T15:46:07.448Z',
    state: 'active',
    iid: 5,
    start_date: null,
    group_id: null,
    name: 'v4.0',
  },
];

export const mockLabels = [
  { id: 74, title: 'Alero', color: '#6235f2', text_color: '#FFFFFF' },
  { id: 9, title: 'Amsche', color: '#581cc8', text_color: '#FFFFFF' },
];

export const mockUsers = [
  {
    id: 31,
    name: 'VSM User2',
    username: 'vsm-user-2-1589776313',
    state: 'active',
    avatar_url:
      'https://www.gravatar.com/avatar/762398957a8c6e04eed16da88098899d?s=80\u0026d=identicon',
    web_url: 'http://127.0.0.1:3001/vsm-user-2-1589776313',
    access_level: 30,
    expires_at: null,
  },
  {
    id: 32,
    name: 'VSM User3',
    username: 'vsm-user-3-1589776313',
    state: 'active',
    avatar_url:
      'https://www.gravatar.com/avatar/f78932237e8a5c5376b65a709824802f?s=80\u0026d=identicon',
    web_url: 'http://127.0.0.1:3001/vsm-user-3-1589776313',
    access_level: 30,
    expires_at: null,
  },
  {
    id: 33,
    name: 'VSM User4',
    username: 'vsm-user-4-1589776313',
    state: 'active',
    avatar_url:
      'https://www.gravatar.com/avatar/ab506dc600d1a941e4d77d5ceeeba73f?s=80\u0026d=identicon',
    web_url: 'http://127.0.0.1:3001/vsm-user-4-1589776313',
    access_level: 30,
    expires_at: null,
  },
];
