import initReportsApp from 'ee/analytics/reports';

document.addEventListener('DOMContentLoaded', () => {
  initReportsApp();
});
