export default Object.freeze({
  LOADING: 'LOADING',
  SUCCESS: 'SUCCESS',
  ERROR: 'ERROR',
  ERROR_DISMISSED: 'ERROR_DISMISSED',
});
